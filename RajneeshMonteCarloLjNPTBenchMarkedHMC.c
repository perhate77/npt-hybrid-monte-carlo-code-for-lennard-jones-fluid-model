#define NDIM  3

#include "in_mddefs.h"
#include "gasdev.h"
#include "ran2.h"
#include "gamdev.h"
#include "maxboltzdev.h"
typedef struct {
  VecR r, rv, ra, rvMD, raMD, rMD;
  //VecR rOld, rvOld, rFlag, mfpt;
} Mol;

void PrintChainProps (FILE *);	//do print the required vale to the file.
void PrintChainProps1 (FILE *);
void PrintPropsToFile (FILE *);
void PrintPropsSTDOUT ();
void monteCarloNPT ( );
void monteCarloMove ( );
void monteCarloVol ( );
void potEnergy ( );
void ApplyPeriodicBoundaryCond ();
void VelocityVerlet (int );


Mol *mol;
int c;
VecR region, vSum;
real MDdeltaT, PI, length, rho, temperature, pressure, beta, rCut, ri3Cut, rrCut, rri, rri3, vir, pSum, rhoSum, vol, deltalnV, deltaR, pCorr, energy, eSum, eCorr, eCut, simulatedPress, KE, PE, TE, PEOld, TEOld;
int moreCycles, nMol, stepAvg, stepCount, stepEquil, stepLimit, sampleCount, sigma;
VecI cells;
int acc, nAcc, nTransAcc, nVolAcc, nTransAtt, nVolAtt;
int molIndex;
long seed, initSeed;
FILE *fp, *fp1, *fp2, *fp3, *fp4;
char infile1[500], infile2[500], outfile[500], infile3[500];




/*NameList nameList[] = {
 NameI (stepAvg),
 NameI (stepEquil),
 NameI (stepLimit),
 NameR (temperature),
 NameR (bondLim),
 NameI (chainLen),
 NameR (MDdeltaT),
 NameR (fricStat),
 NameR (springConst),
};*/


//#####################################
int main (int argc, char **argv)
{ 

 SetParams ();
 SetupJob ();
 fp = fopen ("PressDensityCurve_T_2.0_LJFluidHMC.dat", "w");
 moreCycles = 1;
 while (moreCycles) {
 	SingleStep ();
 	if (stepCount >= stepLimit)	moreCycles = 0;
 }
 
 eSum /= (sampleCount * nMol);
 rhoSum /= sampleCount;
 simulatedPress = (pSum / sampleCount) + (rhoSum * temperature);
 PrintPropsToFile (fp);
 //PrintPropsSTDOUT ();
 fclose (fp);
 return 0;
}


//#####################################
void SingleStep ()
{
 int n, MDsteps, i;
 ++ stepCount;
 real rNum;
 

 MDsteps =40;
 potEnergy ();
 
 

 rNum = ran2(&seed);
 PEOld = energy;
 if (rNum < 0.9)	{
/* if(fmod(stepCount,100)==0)	{*/
/*	if(nTransAcc*(1.0/nTransAtt)<0.5)	        MDdeltaT=MDdeltaT*0.75;*/
/*	else	MDdeltaT=MDdeltaT*1.25;*/
/*	nTransAcc=0;*/
/*	nTransAtt=0;*/
/*}*/
 nTransAtt++;
 InitVels ();
 TEOld = PEOld + KE;
 InitAccels ();
 for (i = 0;i < nMol;i++) VCopy (mol[i].rMD, mol[i].r);
 for (i = 0;i < MDsteps;i++) {
        VelocityVerlet (1);
        ComputeForces ();
        VelocityVerlet (2);  
 }
 monteCarloMove ();
 //printf("%d\t\t%lf\t%lf\t%lf\t%lf\n",stepCount,(PI/6.)*rho, nTransAcc*(1.0/nTransAtt),nVolAcc*(1.0/nVolAtt), MDdeltaT);
 }
 
 else {
 //printf("%d\t\t%lf\t%lf\n",stepCount,(PI/6.)*rho, nTransAcc*(1.0/nTransAtt));
 monteCarloNPT ();
 }

 printf("%d\n", stepCount);
 if (stepCount >= stepEquil ) {
 	EvalProps ();
 	++ sampleCount;
 }
}


//#####################################
void SetParams ()
{
 nMol = 108;
 stepAvg = 1;
 stepEquil = 100000;
 stepLimit = 1100000;
 temperature = 2.0;
 pressure = 4.;
 rho = 0.5;
 deltaR = 0.1;
 deltalnV = 0.1;
 MDdeltaT = 0.001; 
         
 
 real sigmaRSq, sigmaVSq;
 PI = 3.14159265358979;
 //seed = -98765432;
 initSeed = -98765432;
 seed = initSeed;

  
 sigma = 1;
 //rCut = pow (2., 1./6.) * sigma;
 rCut = 2.5;
 rrCut = Sqr (rCut);
 ri3Cut = 1. / (Cube (rCut));
 beta = 1. / (temperature);
 length = pow (nMol/rho,0.333333);
 rri = 1. / rrCut;
 rri3 = Cube (rri);
 eCut = 4. *  rri3 * (rri3 - 1);
 eCorr = 8. * PI * rho * ((ri3Cut * ri3Cut * ri3Cut / 9.) - (ri3Cut / 3.));
 pCorr = (16./3.) *  PI * rho * rho * (((2./3.) * ri3Cut * ri3Cut * ri3Cut) - (ri3Cut));
}


//#####################################
void SetupJob ()
{
 int n;
 AllocArrays ();
 stepCount = 0;
 sampleCount = 0;
 nAcc = 0;
 nTransAcc = 0;
 nTransAtt = 0;
 nVolAcc = 0;
 nVolAtt = 0;
 eSum = 0.0;
 rhoSum = 0.0;
 pSum = 0.0;
 
 InitCoords ();
 potEnergy ();
}


//#####################################
void AllocArrays ()
{
 int k;
 AllocMem (mol, nMol, Mol);
}


//#########################################
void monteCarloNPT ( )
{
 //molIndex = ran2(&seed) * (nMol + 1);
 //printf ("%d\n",molIndex);
 //if (molIndex < nMol)	monteCarloMove ();
 //else	
 monteCarloVol ();
}

//#########################################
void monteCarloMove ( )
{
 int i;
 real deltaE, bdE, randNum;
 nTransAcc++;
  TE = energy + KE;
  if(TE <=TEOld) {
  	for (i = 0; i < nMol; i++) VCopy (mol[i].r, mol[i].rMD);
  }
  else {
  	deltaE = TE - TEOld;
  	bdE = beta * deltaE;
  	randNum = ran2(&seed);
  	if (randNum < exp (-bdE)) {
  		for (i = 0; i < nMol; i++) VCopy (mol[i].r, mol[i].rMD);
  	}
  	else {
  		energy = PEOld;
  		nTransAcc--;
  	}
  }
}

//#########################################
/*void monteCarloMove ( )*/
/*{*/
/* int i;*/
/* VecR dr, rOld, randNumShifted;*/
/* real energyOld, virOld, randNum, deltaE, bdE;*/
/* real s1, s2, s3;*/
/* */
/* energyOld = energy;*/
/* virOld = vir;*/
/* */
/* s1 = (ran2(&seed) - 0.5);*/
/* s2 = (ran2(&seed) - 0.5);*/
/* s3 = (ran2(&seed) - 0.5);*/
/* VSet(randNumShifted, s1, s2, s3); */
/* //randNumShifted.x = (ran2(&seed) - 0.5);*/
/* //randNumShifted.y = (ran2(&seed) - 0.5);*/
/* //randNumShifted.z = (ran2(&seed) - 0.5);*/
/* */
/* VSCopy(dr, deltaR, randNumShifted);*/
/* //dr.x = deltaR * (ran2(&seed) - 0.5);*/
/* //dr.y = deltaR * (ran2(&seed) - 0.5);*/
/* //dr.z = deltaR * (ran2(&seed) - 0.5);*/
/* */
/* VCopy(rOld, mol[molIndex].r);*/
/* //rOld.x = mol[molIndex].r.x;*/
/* //rOld.y = mol[molIndex].r.y;*/
/* //rOld.z = mol[molIndex].r.z;*/
/* */
/* VVAdd(mol[molIndex].r, dr);*/
/* //mol[molIndex].r.x += dr.x;*/
/* //mol[molIndex].r.y += dr.y;*/
/* //mol[molIndex].r.z += dr.z;*/
/* */
/* ApplyPeriodicBoundaryCond ();*/
/* */
/* potEnergy ();*/
/* deltaE = energy - energyOld;*/
/* bdE = beta * deltaE;*/
/* randNum = ran2(&seed);*/
/* if (randNum > exp (-bdE)) {*/
/* 	VCopy(mol[molIndex].r, rOld);*/
/* 	//mol[molIndex].r.x = rOld.x;*/
/* 	//mol[molIndex].r.y = rOld.y;*/
/* 	//mol[molIndex].r.z = rOld.z;*/
/* 	energy = energyOld;*/
/* 	vir = virOld;*/
/* }*/
/*}*/


//#########################################
void monteCarloVol ( )
{
 int i;
 VecR dr, rOld, randNumShifted;
 real energyOld, virOld, randNum, deltaE, bdE, bPdV, divlnV, arg;
 real V0, V, scaleFac, lnV, lnV0, newLength;
 
 energyOld = energy;
 virOld = vir;
 
 V0 = Cube (length);
 lnV0 = log (V0);
 lnV = lnV0 + deltalnV * (ran2(&seed) - 0.5);
 V=exp(lnV);
 
 newLength = pow(V,0.333333333);
 scaleFac = newLength / length;
 
 for (i = 0; i < nMol; i++) {
 	VScale(mol[i].r, scaleFac);
 	//mol[i].r.x *= scaleFac;
 	//mol[i].r.y *= scaleFac;
 	//mol[i].r.z *= scaleFac;
 }
 length = newLength;
 
 rCut *= scaleFac;
 rrCut = Sqr (rCut);
 rho = nMol / (V);
 ri3Cut = 1. / (Cube (rCut));
 rri = 1. / rrCut;
 rri3 = Cube (rri);
 eCut = 4. *  rri3 * (rri3 - 1);
 eCorr = 8. * PI * rho * ((ri3Cut * ri3Cut * ri3Cut / 9.) - (ri3Cut / 3.));
 pCorr = (16./3.) *  PI * rho * rho * (((2./3.) * ri3Cut * ri3Cut * ri3Cut) - (ri3Cut));
 
 potEnergy ();
 deltaE = (energy - energyOld);
 bdE = beta * deltaE;
 bPdV = pressure * (V-V0) * beta;
 divlnV = lnV - lnV0;
 arg = -1. * (bdE + bPdV - ((nMol + 1) * divlnV));
 randNum = ran2(&seed);
 
 if (randNum > exp (arg)) {
 	scaleFac = 1. / scaleFac;
 
 	for (i = 0; i < nMol; i++) {
 		VScale(mol[i].r, scaleFac);
 		//mol[molIndex].r.x *= scaleFac;
 		//mol[molIndex].r.y *= scaleFac;
 		//mol[molIndex].r.z *= scaleFac;
 	}
 	length *= scaleFac;
 
 	rCut *= scaleFac;
 	rrCut = Sqr (rCut);
 	rho = nMol / (V0);
 	ri3Cut = 1. / (Cube (rCut));
 	rri = 1. / rrCut;
 	rri3 = Cube (rri);
 	eCut = 4. *  rri3 * (rri3 - 1);
 	eCorr = 8. * PI * rho * ((ri3Cut * ri3Cut * ri3Cut / 9.) - (ri3Cut / 3.));
 	pCorr = (16./3.) *  PI * rho * rho * (((2./3.) * ri3Cut * ri3Cut * ri3Cut) - (ri3Cut));
 	
 	energy = energyOld;
 	vir = virOld;
 }
}


//#########################################
void potEnergy ( )
{
 VecR dr;
 real rr;
 int j1, j2, n;
 
 energy = 0.;
 vir = 0.;

 //rrCut = Sqr (rCut);
 	
 // Interaction between monomers
 for (j1 = 0; j1 < nMol - 1; j1 ++) {
 	// Excluded volume interaction between monomers
 	for (j2 = j1 + 1; j2 < nMol; j2 ++) {
 		VSub (dr, mol[j1].r, mol[j2].r);
 		if (dr.x >= 0.5 * length)      dr.x -= length;
 		else if (dr.x < -0.5 * length) dr.x += length;
 		if (dr.y >= 0.5 * length)      dr.y -= length;
 		else if (dr.y < -0.5 * length) dr.y += length;
 		if (dr.z >= 0.5 * length)      dr.z -= length;
 		else if (dr.z < -0.5 * length) dr.z += length;
 		
 		//VWrapAll (dr);
 		rr = VLenSq (dr);
 		if (rr < rrCut) {
 			rri = 1. / rr;
 			rri3 = Cube (rri);
 			energy += 4. *  rri3 * (rri3 - 1);
 			//With eCut...
 			//energy += 4. *  rri3 * (rri3 - 1) - eCut;
 			vir += 48. * rri3 * (rri3 - 0.5);
 		}
 	}
 }
 energy += nMol * eCorr;

}
void ComputeForces ( )
{
 VecR dr;
 real fcVal, rr, r;
 int j1, j2, n, i, j;
 
 energy = 0.;
 //vir = 0.;

 for (n = 0; n < nMol; n++)	VZero (mol[n].raMD);


 for (j2 = 0; j2 < nMol - 1; j2 ++) {
        for (j1 = j2+1; j1 < nMol; j1 ++) {
                //if (j1 != j2) {
                        VSub (dr, mol[j2].rMD, mol[j1].rMD);
                        dr.x -= length * round(dr.x / length);
 			dr.y -= length * round(dr.y / length);
 			dr.z -= length * round(dr.z / length);
 			rr = VLenSq (dr);
 			r = sqrt (rr);
 			if(rr < rrCut) {
 		                rri = 1.0 / rr;
 			        rri3 = rri * rri * rri;
 			        
 			        energy += (4. * ( pow(1./(r),12) - pow(1./(r),6) ) );
 			        fcVal = 48.0 * ((rri3 * ((rri3 ) - 0.5) * rri));
 			        VVSAdd (mol[j2].raMD, fcVal, dr);
 			        VVSAdd (mol[j1].raMD, -fcVal, dr);
 		        }
 		//}     
        }
     }
}
//#########################################
void VelocityVerlet (int part)
{
 int n, m;
 real MDdeltaTSq, velMag;
 VecR r1;
 
 MDdeltaTSq = MDdeltaT * MDdeltaT;

 if (part == 1) {
 	for (n = 0; n < nMol; n++) {
 		// position update
 		VSSAdd(r1, MDdeltaT, mol[n].rvMD, 0.5 * MDdeltaTSq, mol[n].raMD);
 		VVAdd (mol[n].rMD, r1);
 		mol[n].rMD.x -= length * floor(mol[n].rMD.x / length);
                mol[n].rMD.y -= length * floor(mol[n].rMD.y / length);
                mol[n].rMD.z -= length * floor(mol[n].rMD.z / length);
 		
 		// velocity update
 		VVSAdd(mol[n].rvMD, 0.5 * MDdeltaT, mol[n].raMD);
 	}
 }
 else {
        KE = 0.;
 	for (n = 0; n < nMol; n++) {
 		VVSAdd (mol[n].rvMD, 0.5 * MDdeltaT, mol[n].raMD);
                KE=KE + 0.5 * VLenSq (mol[n].rvMD) ;
 	}
 	velMag = sqrt(3. * (nMol-1.) * temperature / (2. * KE));
 //if (sampleCount % 100 == 0) 
 //for (n = 0; n < nMol; n ++)	VScale (mol[n].rvMD, velMag);
 }
}
//#########################################
void ApplyPeriodicBoundaryCond ()
{
 if (mol[molIndex].r.x > length)      mol[molIndex].r.x -= length;
 else if (mol[molIndex].r.x < 0) mol[molIndex].r.x += length;
 if (mol[molIndex].r.y > length)      mol[molIndex].r.y -= length;
 else if (mol[molIndex].r.y < 0) mol[molIndex].r.y += length;
 if (mol[molIndex].r.z > length)      mol[molIndex].r.z -= length;
 else if (mol[molIndex].r.z < 0) mol[molIndex].r.z += length;
}


//#########################################
void InitCoords ()
{
 //VecR c, gap;
 int n, nx, ny, nz, i, j, k;
 real s1, s2, s3;

 
 //**********************intialize Particle Position***********************

 nx=0;ny=0;nz=0;
 n = 2;
 while ((n*n*n)<nMol) n++;
 for (i=0; i < nMol; i++) {
 	s1 = ((double)nx+0.5) * length / n;
 	s2 = ((double)ny+0.5) * length / n;
 	s3 = ((double)nz+0.5) * length / n;
 	VSet(mol[i].r, s1, s2, s3); 
 	//mol[i].r.x = ((double)nx+0.5) * length / n;
    	//mol[i].r.y = ((double)ny+0.5) * length / n;
    	//mol[i].r.z = ((double)nz+0.5) * length / n;
   	nx++;
   	if (nx == n) {
   		nx = 0;
   		ny++;
   		if (ny == n) {
   			ny = 0;
   			nz++;
   		}
   	}
   }
}


//#########################################
void EvalProps ()
{ 
 potEnergy ();
 eSum += energy;
 pSum += ((1. / 3.) * vir) / (Cube (length)) + pCorr;
 rhoSum += nMol / (Cube (length));
}


void InitVels ()
{
 int n;
 real ke, s1, s2, s3, velMag;
 KE = 0.;
 VZero (vSum);
 for (n = 0; n < nMol; n ++) {
 	s1 = gasdev(&seed);
 	s2 = gasdev(&seed);
 	s3 = gasdev(&seed);
 	
 	VSet (mol[n].rvMD, s1, s2, s3);
 	VVAdd (vSum, mol[n].rvMD);
 }
 
 for (n = 0; n < nMol; n ++) {
 	VVSAdd (mol[n].rvMD, - 1. / nMol, vSum);
 	KE=KE+0.5 * VLenSq (mol[n].rvMD) ;
 }
 
 velMag = sqrt(3. * (nMol-1.) * temperature / (2. * KE));
 
 for (n = 0; n < nMol; n ++)	VScale (mol[n].rvMD, velMag);
}


//#########################################
void InitAccels ()
{
 int n;
 for (n = 0; n < nMol; n++)	VZero (mol[n].raMD);
}
//#########################################
/*void PrintChainProps (FILE *fp)
{
 int i;
 for(i = 0 ; i < tBins ; i++)
 	fprintf(fp,"%e %e\n",i * deltaT, velCorr[i] / velCorr[0]);
	fflush (fp);
}*/

//#########################################
/*void PrintChainConfig (FILE *fp)
{
	int i;
	for (i = 0; i < chainLen; i ++)
		fprintf (fp, "%f %f\n", chain[i].r.x, chain[i].r.y);
	//for (i = 1; i < chainLen; i += 2)
		//fprintf (fp, "%f %f %d %f %f %f %f\n", chain[i].r.x, chain[i].r.y, 1, chain[i].rv.x, chain[i].rv.y, chain[i].ra.x, chain[i].ra.y);
	//fprintf (fp, "\n\n");
	fflush (fp);
}*/


//#########################################
void PrintPropsSTDOUT ()
{
fprintf(stdout,
	    "Number of particles=\t\t %i\n" "Temperature=\t\t %.5lf\n" "Pressure=\t\t %.5lf\n" "Average Energy=\t\t %.5lf\n" "Density=\t\t %.5lf\n" "Simulated Pressure=\t\t %.5lf\n",nMol, temperature, pressure, eSum, rhoSum, simulatedPress);
}


//#########################################
void PrintPropsToFile (FILE *fp)
{
fprintf(fp,
	    "Number of particles=\t\t %i\n" "Temperature=\t\t %.5lf\n" "Pressure=\t\t %.5lf\n" "Average Energy=\t\t %.5lf\n" "Density=\t\t %.5lf\n" "Simulated Pressure=\t\t %.5lf\n",nMol, temperature, pressure, eSum, rhoSum, simulatedPress);
}


#include "gasdev.c"
#include "ran2.c"
#include "gamdev.c"
#include "in_rand.c"
#include "in_errexit.c"
//#include "in_namelist.c"
#include "maxboltzdev.c"
