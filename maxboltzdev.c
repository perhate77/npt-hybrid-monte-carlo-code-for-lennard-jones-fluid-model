double maxboltzdev (long *idum)
{
double ran2(long *idum);
double prob (double, double);
double t, vMin, vMax, vMean, Var, pMax, v, p, norm, pi, a;
int n;

t = 1.0;	// temperature
a = sqrt(t);
vMin = 0.0;	// minimum velocity
vMax = 5.0;	// maximum velocity
pi = 3.14159265358979;
// If we want velocities v with zero mean and given variance, we need to subtract vMean from v and multiply be variance
vMean = 2.* a * sqrt (2./pi);
Var = t * (3.0*pi - 8.0)/pi;
// Find maximum probability between the range (vMin, vMax)
pMax = 0.58705065269496;
//v = vMin;
/*while (v < vMax) {
	p = prob (v, t);
	if (p > pMax)
		pMax = p;
	v = v + 0.001;
}*/

//print pMax

n = 1;
  while (n) {
	  // generate random velocity between vMin and vMax
	  v = (vMax - vMin) * ran2(idum) + vMin;
	  // generate random probability p between (0, pMax) 
	  p = pMax * ran2(idum);	

	  if ( p < prob(v,a) ) { // if p < prob(v, t) accept it otherwise reject it
	         n = 0;
	   }
	        
  }
  return((v - vMean) * sqrt(1./Var));

}

double prob (double v, double a) {
        double norm, pi;
        pi = 3.14159265358979;
	norm = sqrt(2./pi) / (pow(a, 3.));
	 return(norm * pow(v,2.) * exp (- v*v/(2.* a*a)));
}
