**This is a  Hybrid Monte Carlo code in NPT ensemble of Lennard-Jones fluid model, written in C.**

    	 


 *	 Compile  using: 
 *      gcc filename.c -lm 



 *	 Run :
 *		 nohup ./a.out &
 	 			

![EOSHMCnpt](/uploads/1ed1adc83f9bbb80bab050e08757d754/EOSHMCnpt.png)

Equation of state of the Lennard-Jones fluid as obtained from N, P, T simulations;
isotherms at T=2.0. The solid line: data is taken from Ref. 1 and the squares are the
results from the simulations.

Ref1: D. Frenkel, B. Smit, Understanding molecular simulations: from algorithms to applications
(second ed.), Academic Press, San Diego (2002).
 

 Written By::<br/>
       **Rajneesh Kumar**
 *	Theoretical Sciences Unit, Jawaharlal Nehru Centre for Advanced Scientific Research,
 *	Bengaluru 560064, India.
 *	Email: rajneesh[at]jncasr.ac.in
 *	27 Dec, 2020

