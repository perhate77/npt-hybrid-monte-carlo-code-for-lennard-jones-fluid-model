/*New version based on Marsaglia and Tsang, "A Simple Method for
 * generating gamma variables", ACM Transactions on Mathematical
 * Software, Vol 26, No 3 (2000), p363-372.
*/
double gamdev (long *idum, double a, double b)
{
	// assume a > 0 
	//double ran2(long *idum);
	//double gasdev(long *idum);
	double x, v, u;
	if (a < 1) {
		double u = ran2 (idum);
		return gamdev (idum, 1.0 + a, b) * pow (u, 1.0 / a);
	}
	{
		double d = a - 1.0 / 3.0;
		double c = (1.0 / 3.0) / sqrt (d);
		while (1) {
			do {
				x = gasdev (idum);
				v = 1.0 + c * x;
			}while (v <= 0);
			
			v = v * v * v;
			u = ran2 (idum);
			
			if (u < 1 - 0.0331 * x * x * x * x) 
			break;
			if (log (u) < 0.5 * x * x + d * (1 - v + log (v)))
			break;
		}
		return b * d * v;
	}
}

