//#include "nrutil.h"
//#include "myheader.h"

/*
int randSeedP = 17;

void InitRand (int randSeedI)
{
  struct timeval tv;

  if (randSeedI != 0) randSeedP = randSeedI;
  else {
    gettimeofday (&tv, 0);
    randSeedP = tv.tv_usec;
  }
}

real RandR ()
{
  randSeedP = (randSeedP * IMUL + IADD) & MASK;
  return (randSeedP * SCALE);
}
*/

#if NDIM == 2

void VRand (VecR *p)
{
	p->x = ran2 (&seed);
	p->y = ran2 (&seed);
}

void VRandGaussDev (VecR *p)
{
	p->x = gasdev (&seed);
	p->y = gasdev (&seed);
}

#elif NDIM == 3

void VRand (VecR *p)
{
	p->x = ran2(&seed);;
	p->y = ran2(&seed);;
	p->z = ran2(&seed);;
}

void VRandGaussDev (VecR *p)
{
	p->x = gasdev (&seed);
	p->y = gasdev (&seed);
	p->z = gasdev (&seed);
}

#endif
